const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js");

module.exports.createOrder = (data,reqBody) => {
	return Product.findById(data.productId).then(result =>{
		let newOrder = new Order({
		quantity: reqBody.quantity,
		totalAmount: result.price*reqBody.quantity,
		orderDetails: {
			userId: data.userId,
			productId: data.productId
			}
		});
		let newProduct = {
			stocks: result.stocks-reqBody.quantity,
			isActive: result.stocks-reqBody.quantity == 0 ? false : true
		};
		if(newProduct.stocks>=0){
			return Product.findByIdAndUpdate(data.productId, newProduct).then((product, error) =>{
				return newOrder.save().then((order, error)=>{
					if (error) {
						return false;
					}else{
						return true;
					}
				})				
			})	
		} else {
			return "Out of stock";
		}
	})
}

module.exports.retrieveUserOrder = (data) => {
	return Order.find({orderDetails:{$elemMatch:{userId: data.userId}}}).then((result) =>{
		return result;
	})
}

module.exports.retrieveAllOrder = () => {
	return Order.find({}).then((result) =>{
		return result;
	})
} 