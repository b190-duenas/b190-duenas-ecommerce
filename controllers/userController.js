const User = require("../models/User.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

//User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0){
			return `User with same email:${reqBody.email} already exist`;
		}else{
			return newUser.save().then((user, error) =>{
				if (error) {
					return false;
				}else{
					return true;
				}
			})
		}
	})
};

//User Login
module.exports.loginUser = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		if(result === null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { bearerToken: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	})
};

module.exports.getAllUser = (reqBody) => {
	return User.find({ }).then(result => {
	    for(let x = 0; x < result.length; x++  ){
	    	result[x].password =  ""
        }
	    return result;
	});
};


module.exports.setAsAdmin = (reqParams) => {
	let setAsAdmin = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((product, error)=>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};