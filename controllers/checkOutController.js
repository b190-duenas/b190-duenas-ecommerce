const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const CheckOut = require("../models/CheckOut.js")

module.exports.checkOutOrder = (data,reqBody) => {
	let checkOutAmmount = new CheckOut ({
		checkOutTotalAmount: 0
	});
	return Order.find({orderDetails:{$elemMatch:{userId: data.userId}}}).then((result) =>{
		for(let x = 0; x < result.length; x++){
			checkOutAmmount = {
				userId: data.userId,
				orders: result,
				checkOutTotalAmount: result[x].totalAmount+checkOutAmmount.checkOutTotalAmount
			}
		}
		let done = new CheckOut({
			userId: data.userId,
			orders: result,
			checkOutTotalAmount: checkOutAmmount.checkOutTotalAmount
		})
		return done.save().then((order, error)=>{
			if (error) {
				return false;
			}else{
				return order;
			}
		})
	})
}