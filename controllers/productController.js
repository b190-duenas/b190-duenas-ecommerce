const Product = require("../models/Product.js");

//Retrieve all Product
module.exports.getAllProduct = () =>{
	return Product.find({}).then(result =>{
		return result;
	})
};

//Retrieve all active Product
module.exports.getAllActiveProduct = () =>{
	return Product.find({ isActive: true }).then(result =>{
		return result;
	})
};

//Retrieve single product
module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

//Create Product
module.exports.addProduct = (data) => {
	let newProduct = new Product({
		name : data.name,
		description : data.description,
		price : data.price,
		stocks: data.stocks
	});
	return newProduct.save().then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

//Update Product
module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error)=>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};

//Archive Product
module.exports.archiveProduct = ( reqParams, reqBody ) => {
	let updateActiveField = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error)=>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};