const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	quantity: {
		type: Number,
		required: [true, "quantity is required"]
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn:{
		type: Date,
		default: new Date()
	},
	orderDetails: [
		{
			userId:{
				type: String,
				required: [true, "userId is required"]
			},
			productId:{
				type: String,
				required: [true, "productId is required"]
			}
		}
	]
});

module.exports = mongoose.model("Order", orderSchema);