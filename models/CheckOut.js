const mongoose = require("mongoose");

const checkOutSchema = new mongoose.Schema({
	userId:{
			type: String,
			required: [true, "userId is required"]
		},
	orders: [],
	checkOutTotalAmount : {
		type: Number,
		default: 0
	}
})

module.exports = mongoose.model("CheckOut" , checkOutSchema);