const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js")
const checkOutRoutes = require("./routes/checkOutRoutes.js")

const app = express();

mongoose.connect("mongodb+srv://renzrad:renzrad@wdc028-course-booking.uf7kzlq.mongodb.net/b190-duenas-ecommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

//allows all resources to access our backend application 
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);
app.use("/checkOut", checkOutRoutes);



// process.env.PORT handles the environment of the hosting websites should app be hosted in a website such as Heroku
app.listen(process.env.PORT || 4000, () => {console.log(`API now online at port ${process.env.PORT || 4000}`)});
