const express = require("express");
const router = express.Router();
const checkOutController = require("../controllers/checkOutController.js");
const auth = require("../auth.js");

router.post('/', auth.verify, (req,res) =>{
	let data ={
		userId : auth.decode(req.headers.authorization).id,
	}
	checkOutController.checkOutOrder(data,req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;