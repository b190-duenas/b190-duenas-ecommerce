const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");

router.post('/order',auth.verify, (req,res) =>{
	let data ={
		userId : auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	} else {
		orderController.createOrder(data,req.body).then(resultFromController => res.send(resultFromController));
	}
});

router.get('/allOrders', auth.verify, (req,res) =>{
	let data ={
		userId : auth.decode(req.headers.authorization).id,
	}
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	} else {
		orderController.retrieveUserOrder(data).then(resultFromController => res.send(resultFromController));
	}
})

router.get('/all', auth.verify, (req,res) =>{
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		orderController.retrieveAllOrder().then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router;