const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

//Route for User registration
router.post("/register", ( req, res ) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Authentication
router.post("/login", ( req, res ) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController ));
});

router.post("/getAllUser", auth.verify, ( req, res ) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		userController.getAllUser(req.body).then(resultFromController => res.send(resultFromController ));
	}
});

router.put("/:userId/setAsAdmin", auth.verify, ( req, res ) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController ));
	}
});


module.exports = router;