const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");



// route for getting all products
router.get('/all', (req, res)=>{
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// route for getting all ACTIVE products
router.get('/allActive', (req, res) =>{
	productController.getAllActiveProduct().then(resultFromController => res.send(resultFromController));
});

// route for retrieving a specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for creating a product
router.post("/add", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
});

// route for updating a product
router.put('/:productId', auth.verify, (req, res) =>{
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
});

// route for archiving a product
router.put("/:productId/archive", auth.verify, (req,res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router;